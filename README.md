# cipher-analytical-machine

A program helps to learn how ciphers works.

## Installation

Download from https://gitlab.com/KKlochko/cipher-analytical-machine.

## Usage

You need install java, before run the application.
Run the application with a command:

    $ java -jar cipher-analytical-machine-VERSION-standalone.jar [args]
    $ java -jar cipher-analytical-machine-0.9.15-standalone.jar [args]

## License

Copyright © 2023 Kostiantyn Klochko

Under the GNU Lesser General Public License v3.0 or later.

