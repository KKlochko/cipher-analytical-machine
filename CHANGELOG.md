# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.9.15] - 2023-11-26

### Changed
- Update the CHANGELOG.
- Update the version.
- Update the README.

## [0.9.14] - 2023-11-26

### Added
- Add analyzer for the simple substitution cipher.
- Add the analyzer options and their actions for the simple substiotution.

## [0.9.13] - 2023-11-26

### Fixed
- Fix the test name.

## [0.9.12] - 2023-11-20

### Changed
- Update the CHANGELOG.
- Update the version.
- Update the README.

## [0.9.11] - 2023-11-14

### Added
- Add the function that return all key combinations for a possible key vector.

### Changed
- Rename the function from get-possible-keys to get-all-combinations-for-blocks.

## [0.9.10] - 2023-11-13

### Added
- Add a function to get all permutations for a block.

## [0.9.9] - 2023-11-12

### Added
- Add a function to convert a count map to a reversed map.
- Add a function to get the ordered sequence that can form a key.

## [0.9.8] - 2023-11-11

### Added
- Add functions to get possible keys.

## [0.9.7] - 2023-11-03

### Added
- Add the default frequency factory.

## [0.9.6] - 2023-11-01

### Added
- Add int-string? to check if a string is a number.
- Add key? to check if a string is a gamma key.
- Add key validation for Caesar and Gamma ciphers.

### Changed
- Rename unsigned-int? to unsigned-int-string?

## [0.9.5] - 2023-10-31
## [0.9.5] - 2023-10-31

### Added
- Add the option for the gamma cipher.

## [0.9.4] - 2023-10-31

### Fixed
- Fix the key parsing in the gamma actions.

## [0.9.3] - 2023-10-31

### Added
- Add the action of encryption for the gamma cipher.
- Add the action of decryption for the gamma cipher.

## [0.9.2] - 2023-10-29

### Added
- Add a function to encrypt a message with the gamma cipher.
- Add a function to decrypt a message with the gamma cipher.

## [0.9.1] - 2023-10-28

### Changed
- Refactor the simple substitute to use the decrypt-by-table function. 

## [0.9.0] - 2023-10-27

### Added
- Add the gamma generator.
- Add the functions to encrypt and decrypt with the gamma cipher.

## [0.8.5] - 2023-10-25

### Added
- Add the main for the cli module.

## [0.8.4] - 2023-10-24

### Changed
- Update the simple substitution cipher to split the decrypt-message.

## [0.8.3] - 2023-10-24

### Added
- Add functions to make a frequency map.

## [0.8.2] - 2023-10-23

### Added
- Add a function to invert the table in a decoded json.
- Add the parser to parse the string as an integer if it's needed.
- Add the simple substitution cipher.

## [0.8.1] - 2023-10-22

### Added
- Add a generator for the sorted substitution table.
- Add the parsers to parse keys.

### Changed
- Update the simple substitution cipher to split the functions.

## [0.8.0] - 2023-10-21

### Added
- Add the simple substitution cipher.

## [0.7.2] - 2023-10-21

### Changed
- Update the CHANGELOG.
- Update the version.
- Update the README.
- Update the CI/CD configuration to publish the artifacts to the release.
- Update the diffs in the CHANGELOG.

## [0.7.1] - 2023-10-20

### Removed
- Remove the test for FrequencyAnalyzer.
- Remove the option for FrequencyAnalyzer.

## [0.7.0] - 2023-10-20

### Changed
- Refactor to move actions to separated files.

## [0.6.3] - 2023-10-14

### Added
- Add the IsNonsense interface for FrequencyAnalyzer.

### Changed
- Update the frequency analyzer.

## [0.6.2] - 2023-10-14

### Added
- Add the analyzer options and their actions.
- Add the available ciphers and analyzer for the help.
- Add validations to check if a cipher or an analyzer is available.

### Changed
- Refactor to move options to a separated file.

## [0.6.1] - 2023-10-13

### Added
- Add the function to make a frequency string from a map.

### Fixed
- Fix the bug that the default language symbols override the symbols.

### Changed
- Update the FrequencyAnalyzer to use the frequency string instead.

## [0.6.0] - 2023-10-12

### Fixed
- Fix the typo "frequences" to frequencies.

### Changed
- Refactor the project structure.

## [0.5.5] - 2023-10-09

### Added
- Add the Gitlab CI/CD configuration.

## [0.5.4] - 2023-10-05

### Added
- Add the java source path.
- Add the Caesar decrypted interface for Java.
- Add a frequency analyzer as a stub.
- Add the clojure wrapper for the frequency analizer and its test.

## [0.5.3] - 2023-10-05

### Added
- Add the CHANGELOG.

## [0.5.2] - 2023-10-02

### Fixed
- Fix checking for file existence only when the options are set.
- Fix writing to a file only when the option is set.

## [0.5.1] - 2023-09-28

### Added
- Add the output option to save into a file.
- Add the file errors to prevent using an invalid file path.

## [0.5.0] - 2023-09-22

### Changed
- Update symbol defaults, factories to have only lower alphabets.
- Update the Caesar to be case-insensitive and skip unknown symbols.
- Update Caesar Analyzers to be case-insensitive and testable.

## [0.4.3] - 2023-09-21

### Added
- Add the language option for symbols.

### Changed
- Update the CLI to have separated actions and an option initializer.

## [0.4.2] - 2023-09-18

### Added
- Add tests for the cipher-analyzers module.

### Changed
- Rename the cipher_analyzer module to cipher-analyzers.

## [0.4.1] - 2023-09-17

### Added
- Add symbol factories for punctuation, space, defaults symbols.
- Add the action-type to distinguish the actions.
- Add the symbol option and its default.
- Add the cracking option.

### Changed
- Rename the language_analyzer module to language-analyzer.
- Update the nonsense for the is-nonsense? test.
- Rename the symbol_frequences module to symbol-frequences.

## [0.4.0] - 2023-09-15

### Added
- Add Chi-squared Statistic function to analyze texts.
- Add the caesar-analyzers to get the plaintext from a ciphertext.

## [0.3.5] - 2023-09-14

### Added
- Add the frequences of symbols as defaults.

## [0.3.4] - 2023-09-13

### Added
- Add a simple language analyzers.

## [0.3.3] - 2023-09-12

### Added
- Add an option to read a message from a file.

## [0.3.2] - 2023-09-11

### Added
- Add the CI/CD configuration.

## [0.3.1] - 2023-09-11

### Added
- Add factories for symbols.
- Add tests for factories.

## [0.3.0] - 2023-09-10

### Added
- Add a simple cli interface.

## [0.2.0] - 2023-09-09

### Added
- Add a implementation of Caesar cipher.

## [0.1.0] - 2023-09-09

### Added
- Project initialization.

[Unreleased]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.9.15...main?from_project_id=50218541&straight=false
[0.9.15]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.9.14...0.0.9.15?from_project_id=50218541&straight=false
[0.9.14]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.9.13...0.0.9.14?from_project_id=50218541&straight=false
[0.9.13]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.9.12...0.0.9.13?from_project_id=50218541&straight=false
[0.9.12]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.9.11...0.0.9.12?from_project_id=50218541&straight=false
[0.9.11]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.9.10...0.0.9.11?from_project_id=50218541&straight=false
[0.9.10]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.9.9...0.9.10?from_project_id=50218541&straight=false
[0.9.9]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.9.8...0.9.9?from_project_id=50218541&straight=false
[0.9.8]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.9.7...0.9.8?from_project_id=50218541&straight=false
[0.9.7]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.9.6...0.9.7?from_project_id=50218541&straight=false
[0.9.6]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.9.5...0.9.6?from_project_id=50218541&straight=false
[0.9.5]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.9.4...0.9.5?from_project_id=50218541&straight=false
[0.9.4]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.9.3...0.9.4?from_project_id=50218541&straight=false
[0.9.3]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.9.2...0.9.3?from_project_id=50218541&straight=false
[0.9.2]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.9.1...0.9.2?from_project_id=50218541&straight=false
[0.9.1]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.9.0...0.9.1?from_project_id=50218541&straight=false
[0.9.0]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.8.5...0.9.0?from_project_id=50218541&straight=false
[0.8.5]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.8.4...0.8.5?from_project_id=50218541&straight=false
[0.8.4]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.8.3...0.8.4?from_project_id=50218541&straight=false
[0.8.3]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.8.2...0.8.3?from_project_id=50218541&straight=false
[0.8.2]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.8.1...0.8.2?from_project_id=50218541&straight=false
[0.8.1]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.8.0...0.8.1?from_project_id=50218541&straight=false
[0.8.0]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.7.2...0.8.0?from_project_id=50218541&straight=false
[0.7.2]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.7.1...0.7.2?from_project_id=50218541&straight=false
[0.7.1]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.7.0...0.7.1?from_project_id=50218541&straight=false
[0.7.0]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.6.3...0.7.0?from_project_id=50218541&straight=false
[0.6.3]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.6.2...0.6.3?from_project_id=50218541&straight=false
[0.6.2]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.6.1...0.6.2?from_project_id=50218541&straight=false
[0.6.1]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.6.0...0.6.1?from_project_id=50218541&straight=false
[0.6.0]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.5.5...0.6.0?from_project_id=50218541&straight=false
[0.5.5]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.5.4...0.5.5?from_project_id=50218541&straight=false
[0.5.4]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.5.3...0.5.4?from_project_id=50218541&straight=false
[0.5.3]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.5.2...0.5.3?from_project_id=50218541&straight=false
[0.5.2]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.5.1...0.5.2?from_project_id=50218541&straight=false
[0.5.1]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.5.0...0.5.1?from_project_id=50218541&straight=false
[0.5.0]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.4.3...0.5.0?from_project_id=50218541&straight=false
[0.4.3]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.4.2...0.4.3?from_project_id=50218541&straight=false
[0.4.2]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.4.1...0.4.2?from_project_id=50218541&straight=false
[0.4.1]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.4.0...0.4.1?from_project_id=50218541&straight=false
[0.4.0]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.3.5...0.4.0?from_project_id=50218541&straight=false
[0.3.5]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.3.4...0.3.5?from_project_id=50218541&straight=false
[0.3.4]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.3.3...0.3.4?from_project_id=50218541&straight=false
[0.3.3]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.3.2...0.3.3?from_project_id=50218541&straight=false
[0.3.2]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.3.1...0.3.2?from_project_id=50218541&straight=false
[0.3.1]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.3.0...0.3.1?from_project_id=50218541&straight=false
[0.3.0]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.2.0...0.3.0?from_project_id=50218541&straight=false
[0.2.0]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.1.0...0.2.0?from_project_id=50218541&straight=false
[0.1.0]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/commit/a7a715b94276fc8f46fb6a44139636d27b551599

