(defproject cipher-analytical-machine "0.9.15"
  :description "A program helps to learn how ciphers works."
  :url "https://gitlab.com/KKlochko/cipher-analytical-machine"
  :license {:name "LGPL"
            :url "http://www.gnu.org/licenses/lgpl-3.0.txt"}
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [org.clojure/tools.cli "1.0.219"]
                 [org.apache.tika/tika-core "1.28.5"]
                 [org.apache.tika/tika-langdetect "1.28.5"]
                 [cheshire "5.12.0"]
                 [org.clojure/math.combinatorics "0.2.0"]]
  :main ^:skip-aot cipher-analytical-machine.core
  :java-source-paths ["src/main/java"]
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}})
