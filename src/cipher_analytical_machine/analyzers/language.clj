(ns cipher-analytical-machine.analyzers.language
  (:import [org.apache.tika.language LanguageIdentifier])
  (:gen-class))

(defn detect-language
  "Return a identified language."
  [text]
  (-> (new LanguageIdentifier text)
      .getLanguage))

(defn is-language?
  "Check if the text written a language."
  [text language-id]
  (= (detect-language text)
     language-id))

(defn is-nonsense?
  "Check if the text written a language is not a language."
  [text language-id]
  (not (is-language? text language-id)))

