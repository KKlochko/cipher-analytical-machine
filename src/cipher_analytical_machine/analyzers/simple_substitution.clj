(ns cipher-analytical-machine.analyzers.simple-substitution
  (:require [clojure.string :as cs]
            [clojure.math.combinatorics :as comb]
            [cipher-analytical-machine.symbols.frequencies :as frequencies]
            [cipher-analytical-machine.ciphers.simple-substitution :as ss]
            [cipher-analytical-machine.analyzers.analyzers :as analyzers]
            [cipher-analytical-machine.analyzers.analyzers :as ca]
            [cipher-analytical-machine.analyzers.caesar :as caesar])
  (:gen-class))

(defn get-possible-key-vector
  "Convert a possible key map to a vector of possible keys. For example, if you have a map: {\\a [\\a \\b \\c] \\b [\\b \\c] \\c [\\a \\c]}, then the vector is [[\\a \\b \\c] [\\b \\c] [\\a \\c]]."
  [possible-key-map symbols]
  (reduce
   (fn [acc el]
     (conj acc (get possible-key-map el)))
   [] symbols))

(defn get-possible-key-vector-from-reversed-count-map
  "Convert a reversed count map to a vector. Example, from {2 [\\a \\b] 1 [\\d] 3 [\\e]} to [[\\e] [\\a \\b] [\\d]])."
  [reversed-count-map]
  (->>  reversed-count-map
        (into [])
        (sort-by #(- (first %)))
        (map second)
        (into [])))

(defn get-all-permutation-for-block
  "Return a vector for all possible blocks that can be made from a vector of symbols."
  [symbols]
  (->> (comb/permutations symbols)
       (map cs/join)
       (into [])))

(defn get-all-combinations-for-blocks
  "Return the all combination of blocks in a possible key vector. For example, if you have a map: {\\a [\\a \"bf\" \\c] \\b [\"bf\" \\c] \\c [\\a \\c]}, then the vector is [[\\a \"bf\" \\c] [\"bf\" \\c] [\\a \\c]] and keys (\"abfa\" \"abfc\" ...)."
  [possible-key-vector]
  (->> (apply comb/cartesian-product possible-key-vector)
       (map cs/join)))

(defn get-possible-key-combinations
  "Return possible keys for a possible key vector. For example, if you have a map: {\\a [\\a \\b \\c] \\e [\\f \\g] \\c [\\a \\c]}, then the keys are (\"abcefg\" \"abcegf\" ...)."
  [possible-key-vector]
  (->> (map get-all-permutation-for-block possible-key-vector)
       (get-all-combinations-for-blocks)))

(defn get-possible-keys
  "Return keys for a cipher text. A key is a map {\\a \\e, ...} to decrypt a text."
  [cipher-text frequency-table]
  (let [freq-symbol-str (frequencies/map-to-string frequency-table) ; "etaoinsrhldcumfpgwybvkxjqz"
        char-map (-> (analyzers/count-characters cipher-text)
                     (analyzers/reverse-count-characters))]
    (map (fn [comb] (zipmap comb freq-symbol-str))
         (-> (get-possible-key-vector-from-reversed-count-map char-map)
             (get-possible-key-combinations)))))


(defn get-plain-data
  "Return the plain data (score, text, key) from a ciphertext (a string of character). The function is case-insensitive."
  [cipher-text letter-frequencies]
  (let [cipher-text (cs/lower-case cipher-text)
        keys (get-possible-keys cipher-text letter-frequencies)]
    (->> (map (fn [key] [(ss/decrypt-message-by-symbol-table key cipher-text) key]) keys)
         (map (fn [[text key]] [(ca/chi-squared-statistic text letter-frequencies) text key]))
         )))

(defn get-plaintext-and-key
  "Return the possible plaintext from a ciphertext that encoded with numbers. The function is case-insensitive."
  [cipher-text letter-frequencies]
  (let [maybe-key   (->> (frequencies/map-to-string letter-frequencies)
                         (ss/generate-sorted-substitution-table))
        cipher-text (->> (cs/split cipher-text #",")
                         (map #(Integer/parseInt %))
                         (ss/decrypt-message-by-symbol-table maybe-key))]

    (->> (get-plain-data cipher-text letter-frequencies)
         (map (fn [el] (drop 1 el)) ))))

