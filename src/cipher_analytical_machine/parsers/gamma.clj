(ns cipher-analytical-machine.parsers.gamma
  (:gen-class))

(defn key?
  "Return true if the string is an key of three integers."
  [str]
  (if (re-matches #"[+-]?\b\d+\b,[+-]?\b\d+\b,[+-]?\b\d+\b" str)
    true false))

