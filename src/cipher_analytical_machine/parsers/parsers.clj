(ns cipher-analytical-machine.parsers.parsers
  (:require [cheshire.core :as cc])
  (:gen-class))

(defn unsigned-int-string?
  "Return true if the string is an unsigned integer."
  [str]
  (if (re-matches #"\d+" str)
    true false))

(defn int-string?
  "Return true if the string is an signed integer."
  [str]
  (if (re-matches #"[+-]?\b\d+\b" str)
    true false))

(defn parse-unsigned-int
  "Return an integer if the argument is an integer."
  [str-or-int]
  (cond
    (int? str-or-int)
    str-or-int

    (unsigned-int-string? str-or-int)
    (Integer/parseInt str-or-int)

    :else
    nil))

