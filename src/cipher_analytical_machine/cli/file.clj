(ns cipher-analytical-machine.cli.file
  (:require [clojure.java.io :as io])
  (:gen-class))

(defn is-file-exists?
  "Checks that a file is exists"
  [file-path]
  (-> (io/file file-path)
      (.exists)))

(defn is-file-not-exists?
  "Checks that a file is not exists"
  [file-path]
  (not (is-file-exists? file-path)))

(defn read-file [file-path]
  (slurp file-path))

(defn write-file [file-path content]
  (spit file-path content))

