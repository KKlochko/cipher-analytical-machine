(ns cipher-analytical-machine.cli.options
  (:require
   [cipher-analytical-machine.symbols.factories :as sf]
   [clojure.string :as cs])
  (:gen-class))

(defn option-values-as-row
  "Return the formatted string of values for an option."
  [option values]
  (str option ": " (cs/join ", " values)))

(defn option-values-as-list
  "Return the formatted string of values for an option as a multiline list."
  [option values]
  (str option ": \n - " (cs/join "\n - " values)))

(defn map-of-option-value-as-list
  "Return the formatted string of values for an option as a multiline list.
   The values are a map of pairs (suboption and its options)."
  [option values]
  (let [values (map #(apply option-values-as-row %) values)]
    (option-values-as-list option values)))

(def cipher-options
  #{"Caesar" "Simple substitution and Caesar" "Gamma"})

(def default-cipher "Caesar")

(defn get-available-cipher-options-as-string
  "Return the formatted string of analyzers options."
  []
  (option-values-as-row "Ciphers" cipher-options))

(def analyzers-options
  {"Caesar" #{"Chi^2" "Frequency"}
   "Simple substitution and Caesar" #{"Chi^2" "Frequency"}})

(defn get-available-analyzers-options-as-string
  "Return the formatted string of analyzers options."
  []
  (map-of-option-value-as-list "Analyzers" analyzers-options))

(def language-options
  #{"en", "uk"})

(def default-symbols
  (sf/default-symbol-factory "en"))

(def cli-options
  [["-m" "--message MESSAGE" "The message will be encrypted or decrypted."]
   ["-M" "--message-file MESSAGE-FILE" "The file contains the message that will be encrypted or decrypted."]
   ["-k" "--key KEY" "The key will be used to encrypt or decrypt."]
   ["-c" "--cipher CIPHER" "The cipher will be used to encrypt or decrypt a message."
    :default default-cipher]
   ["-e" "--encrypt" "Encrypt the message."]
   ["-d" "--decrypt" "Decrypt the message."]
   ["-s" "--symbols SYMBOLS" "The string will be used as a set of symbols for a cipher."
    :default default-symbols]
   ["-l" "--language CODE" "The string will be used to set a default symbols for a cipher."
    :validate [#(contains? language-options %) "Must be a code from the list: en, uk!!!"]]
   ["-C" "--cracking" "Cracking the encrypted message."]
   ["-a" "--analyzer ANALYZER" "The way of cracking."]
   ["-O" "--output-file OUTPUT-FILE" "Save the program output to a file."]
   ["-h" "--help"]])

