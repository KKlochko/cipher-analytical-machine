(ns cipher-analytical-machine.cli.actions.cryptography
  (:require
   [clojure.string :as cs]
   [cipher-analytical-machine.ciphers.caesar :as caesar]
   [cipher-analytical-machine.ciphers.simple-substitution :as ss]
   [cipher-analytical-machine.ciphers.gamma :as gamma]
   [cipher-analytical-machine.parsers.parsers :as ps]
   [cipher-analytical-machine.parsers.simple-substitution :as pss])
  (:gen-class))

(defn caesar-actions
  [options arguments action-type]
  (let [message (:message options)
        key (Integer/parseInt (:key options))
        symbols (:symbols options)]
    (cond
      (= action-type :encrypt)
      (caesar/encrypt-message message key symbols)

      (= action-type :decrypt)
      (caesar/decrypt-message message key symbols))))

(defn simple-substitution-with-caesar-actions
  [options arguments action-type]
  (let [message (:message options)
        key (:key options)
        symbols (:symbols options)]
    (cond
      (= action-type :encrypt)
      (let [data (pss/generate-table-or-decode-json key symbols)
            key (ps/parse-unsigned-int (get data "key"))
            substitution-table (get data "table")]
        (println (pss/encode-key-and-substitution-table-to-json key substitution-table))
        (ss/encrypt-message-with-caesar message key substitution-table symbols))

      (= action-type :decrypt)
      (let [data (-> (pss/decode-key-and-substitution-table-from-json key)
                     (pss/invert-table-in-map))
            key (get data "key")
            substitution-table (get data "table")]
        (ss/decrypt-message-with-caesar message key substitution-table symbols)))))

(defn gamma-actions
  [options arguments action-type]
  (let [message (:message options)
        key (->> (cs/split (:key options) #",")
                 (map #(Integer/parseInt %))
                 (into []))
        symbols (:symbols options)]
    (cond
      (= action-type :encrypt)
      (gamma/encrypt-message key symbols message)

      (= action-type :decrypt)
      (gamma/decrypt-message key symbols message))))

(defn cryptography-actions
  [options arguments action-type]
  (let [cipher (:cipher options)]
    (cond
      (= cipher "Caesar")
      (caesar-actions options arguments action-type)

      (= cipher "Simple substitution and Caesar")
      (simple-substitution-with-caesar-actions options arguments action-type)

      (= cipher "Gamma")
      (gamma-actions options arguments action-type))))

