(ns cipher-analytical-machine.cli.actions.cracking
  (:require
   [cipher-analytical-machine.analyzers.caesar :as caesar-analyzers]
   [cipher-analytical-machine.analyzers.simple-substitution :as ss-analyzers]
   [cipher-analytical-machine.symbols.frequencies :as symbol-frequencies])
  (:gen-class))

(defn cracking-caesar
  [options arguments]
  (let [analyzer (:analyzer options)
        message (:message options)
        symbols (:symbols options)
        frequencies (-> (get options :language "en")
                        (symbol-frequencies/default-frequency-factory))]
    (cond
      (= analyzer "Chi^2")
      (caesar-analyzers/get-plaintext message symbols frequencies)

      :else
      (caesar-analyzers/get-plaintext message symbols frequencies))))

(defn cracking-simple-substitution-with-caesar
  [options arguments]
  (let [analyzer (:analyzer options)
        message (:message options)
        symbols (:symbols options)
        frequencies (-> (get options :language "en")
                        (symbol-frequencies/default-frequency-factory))]
    (cond
      (= analyzer "Chi^2")
      (ss-analyzers/get-plaintext-and-key message frequencies)

      :else
      (ss-analyzers/get-plaintext-and-key message frequencies))))

(defn cracking-actions
  [options arguments action-type]
  (let [cipher (:cipher options)]
    (cond
      (= cipher "Caesar")
      (cracking-caesar options arguments)

      (= cipher "Simple substitution and Caesar")
      (cracking-simple-substitution-with-caesar options arguments))))

