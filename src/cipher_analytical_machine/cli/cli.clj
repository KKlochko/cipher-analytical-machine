(ns cipher-analytical-machine.cli.cli
  (:require
   [cipher-analytical-machine.parsers.parsers :as ps]
   [cipher-analytical-machine.parsers.gamma :as gamma-ps]
   [cipher-analytical-machine.cli.file :as file]
   [cipher-analytical-machine.symbols.factories :as sf]
   [cipher-analytical-machine.cli.options :as options]
   [clojure.string :as cs]
   [cipher-analytical-machine.cli.actions.cryptography :as cryptography]
   [cipher-analytical-machine.cli.actions.cracking :as cracking]
   [clojure.tools.cli :refer [parse-opts]])
  (:gen-class))

(defn usage [options-summary]
  (->> ["This is cipher-analytical-machine. It can help you to learn how ciphers works."
        ""
        "Usage: cipher-analytical-machine [options]"
        ""
        "Options:"
        options-summary
        ""
        "Available values"
        ""
        (options/get-available-cipher-options-as-string)
        ""
        (options/get-available-analyzers-options-as-string)
        ""]
       (cs/join \newline)))

(defn error-msg [errors]
  (str "The following errors occurred while parsing your command:\n\n"
       (cs/join \newline errors)))

(defn map-has-keys?
  "Return true if all keys in the map."
  [map keys]
  (every? #(contains? map %) keys))

(defn validate-args
  "Validate command line arguments. Either return a map indicating the program
  should exit (with an error message, and optional ok status), or a map
  indicating the action the program should take and the options provided."
  [args]
  (let [{:keys [options arguments errors summary]} (parse-opts args options/cli-options)]
    (cond
      (:help options)
      {:exit-message (usage summary) :ok? true}

      errors
      {:exit-message (error-msg errors)}

      (map-has-keys? options [:encrypt, :decrypt])
      {:exit-message (error-msg ["You can't use enctypt and decrypt mode at the same time!!!"])}

      (map-has-keys? options [:message, :message-file])
      {:exit-message (error-msg ["You can't use message and message-file options at the same time!!!"])}

      (and (contains? options :message-file)
           (file/is-file-not-exists? (:message-file options)))
      {:exit-message (error-msg ["Please, check the path to the message file!!!"])}

      (and (contains? options :output-file)
           (file/is-file-exists? (:output-file options)))
      {:exit-message (error-msg ["Please, choose another file!!! Overwriting was prevented!!!"])}

      (false? (contains? options/cipher-options (:cipher options)))
      {:exit-message (error-msg ["Please, choose an available cipher!!!\n"
                                 (options/get-available-cipher-options-as-string)])}

      (and (map-has-keys? options [:cipher, :cracking, :analyzer])
           (false? (contains?
             (get options/analyzers-options (:cipher options)) (:analyzer options))))
      {:exit-message (error-msg ["Please, choose an available analyzer!!!\n"
                                 (options/get-available-analyzers-options-as-string)])}

      (and (map-has-keys? options [:cipher, :key])
           (or (contains? options :message)
               (contains? options :message-file))
           (or (contains? options :encrypt)
               (contains? options :decrypt)))
      (cond
        (and (= (:cipher options) "Caesar")
             (false? (ps/int-string? (:key options))))
        {:exit-message (error-msg ["Please, use the key format: [+-](integer)!!! Example: 10, +9, -7."])}

        (and (= (:cipher options) "Gamma")
             (false? (gamma-ps/key? (:key options))))
        {:exit-message (error-msg ["Please, use the key format: [+-](integer),[+-](integer),[+-](integer) !!! Example: 10,+9,-7."])}


        (contains? options :encrypt)
        {:options options :arguments arguments :action-type :encrypt}

        (contains? options :decrypt)
        {:options options :arguments arguments :action-type :decrypt})

      (and (map-has-keys? options [:cipher, :cracking])
           (or (contains? options :message)
               (contains? options :message-file)))
      {:options options :arguments arguments :action-type :cracking}

      :else
      {:exit-message (usage summary)})))

(defn exit
  [exit-message status]
  (println exit-message)
  (System/exit (if status 0 1)))

(defn get-or-load-option
  "Return the option value or load the value from a file."
  [options arguments option]
  (let [file-option (keyword (str (name option) "-file"))]
    (cond
      (contains? options option)
      (option options)

      (contains? options file-option)
      (file/read-file (file-option options)))))

(defn set-option
  "Set a option in the option map."
  [options option value]
  (-> options
      (assoc option value)))

(defn set-symbols
  "Set defaults symbols for a language."
  [options]
  (if (contains? options :language)
    (set-option options :symbols
                (sf/default-symbol-factory (:language options)))
    options))

(defn load-and-set-option
  "Load the option and set it."
  [options option]
  (->> (get-or-load-option options nil option)
       (set-option options option)))

(defn load-all-options
  "Load all options."
  [options]
  (-> options
      (set-symbols)
      (load-and-set-option :message)))

(defn save-output
  "Save the output to a file"
  [output options]
  (let [file-path (:output-file options)]
    (file/write-file file-path output)))

(defn show-and-save-output
  "Print and save the output to a file if needed"
  [output options]
  (when (contains? options :output-file)
    (save-output output options))
  (println output))

(defn actions
  [options arguments action-type]
  (let [options (load-all-options options)]
    (->
     (cond
       (contains? #{:encrypt :decrypt} action-type)
       (cryptography/cryptography-actions options arguments action-type)

       (= action-type :cracking)
       (cracking/cracking-actions options arguments action-type))

     (show-and-save-output options))))

(defn cli-main
  [args]
  (let [{:keys [options arguments action-type exit-message ok?]} (validate-args args)]
    (if exit-message
      (exit exit-message ok?)
      (actions options arguments action-type))))

