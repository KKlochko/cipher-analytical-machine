(ns cipher-analytical-machine.core
  (:require
   [cipher-analytical-machine.cli.cli :as cli])
  (:gen-class))

(defn -main
  [& args]
  (cli/cli-main args))

