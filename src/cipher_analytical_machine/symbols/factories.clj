(ns cipher-analytical-machine.symbols.factories
  (:require [clojure.string :as cs])
  (:gen-class))

(defn english-alphabet-factory
  "Return the English alphabet with only lowercase letters"
  []
  (reduce
   (fn [acc val] (str acc (char val)))
   ""
   (range (int \a) (inc (int \z)))))

(defn ukrainian-alphabet-factory
  "Return the Ukrainian alphabet with only lowercase letters"
  []
  "абвгґдеєжзиіїйклмнопрстуфхцчшщьюя")

(defn digit-set-factory
  "Return digits"
  []
  "0123456789")

(defn punctuation-symbol-factory
  "Return punctuation symbols"
  []
  ".,:;-!?")

(defn space-symbol-factory
  "Return punctuation symbols"
  []
  " \t\n")

(defn default-symbol-factory
  "Return a default string of symbols for a language"
  [language-code]
  (cond
    (= language-code "en")
    (str (english-alphabet-factory)
         \ )

    (= language-code "uk")
    (str (ukrainian-alphabet-factory)
         \ )

    :else
    (default-symbol-factory "en")))

