(ns cipher-analytical-machine.symbols.frequencies
  (:gen-class))

(defn map-to-string
  "Convert the map of frequencies to the frequency string. Letters are sorted by frequency in descending order."
  [frequency-map]
  (->> frequency-map
       (reduce (fn [acc el] (conj acc el)) [])
       (sort-by last)
       (map first)
       (reverse)
       (apply str)))

(def english-letter-frequencies {
   \a 0.0804
   \b 0.0154
   \c 0.0306
   \d 0.0399
   \e 0.1251
   \f 0.0230
   \g 0.0196
   \h 0.0549
   \i 0.0726
   \j 0.0016
   \k 0.0067
   \l 0.0414
   \m 0.0253
   \n 0.0709
   \o 0.0760
   \p 0.0200
   \q 0.0011
   \r 0.0612
   \s 0.0654
   \t 0.0925
   \u 0.0271
   \v 0.0099
   \w 0.0192
   \x 0.0019
   \y 0.0173
   \z 0.0009
})

(def ukrainian-letter-frequencies {
   \а 0.064
   \б 0.013
   \в 0.046
   \г 0.013
   \ґ 0.0001
   \д 0.027
   \е 0.042
   \є 0.005
   \ж 0.007
   \з 0.020
   \и 0.055
   \і 0.044
   \ї 0.010
   \й 0.009
   \к 0.033
   \л 0.027
   \м 0.029
   \н 0.068
   \о 0.086
   \п 0.025
   \р 0.043
   \с 0.037
   \т 0.045
   \у 0.027
   \ф 0.003
   \х 0.011
   \ц 0.010
   \ч 0.011
   \ш 0.005
   \щ 0.004
   \ь 0.016
   \ю 0.008
   \я 0.019
   \  0.138
})

(defn default-frequency-factory
  "Returns a default map of letter frequencies for a language"
  [language-code]
  (cond
    (= language-code "en")
    english-letter-frequencies

    (= language-code "uk")
    ukrainian-letter-frequencies

    :else
    english-letter-frequencies))

