(ns cipher-analytical-machine.ciphers.simple-substitution
  (:require [clojure.string :as cs]
            [cipher-analytical-machine.ciphers.caesar :as caesar])
  (:gen-class))

(defn shuffled-numbers
  "Generate the shuffled order for integer list."
  [size]
  (-> size
      (take (range))
      (shuffle)))

(defn generate-substitution-table
  "Generate the map (char, int) for the substittion."
  [symbols]
  (->> (count symbols)
       (shuffled-numbers)
       (zipmap symbols)))

(defn generate-sorted-substitution-table
  "Generate the map (char, int) for the substittion. BUT a value is the index of a symbol (from 0)."
  [symbols]
  (-> (count symbols)
      (take (range))
      (zipmap symbols)))

(defn find-value-in-table
  "It uses the substitution table to find the value of a char or a number."
  [char substitution-table]
  (get substitution-table char))

(defn decrypt-by-table
  "Decrypt a message by the substitution-table"
  [substitution-table message]
  (map (fn [char] (find-value-in-table char substitution-table)) message))

(defn encrypt-message
  "Encrypt a message using the simple substitution cipher. The function is case-insensitive. If a symbol isn't in the symbols list, then it will be removed."
  [message substitution-table]
  (->> message
       (decrypt-by-table substitution-table)
       (remove nil?)
       (cs/join \,)))

(defn encrypt-message-with-caesar
  "Encrypt a message using the simple substitution cipher and the Caesar cipher. The function is case-insensitive. If a symbol isn't in the symbols list, then it will be removed."
  [message key substitution-table symbols]
  (-> message
      (caesar/encrypt-message key symbols)
      (encrypt-message substitution-table)))

(defn decrypt-message-by-symbol-table
  "Decrypt a message using the simple substitution cipher. The function is case-insensitive. The substitution-table must be (symbols, symbols)."
  [substitution-table message]
  (->> message
       (decrypt-by-table substitution-table)
       (remove nil?)
       (cs/join)))

(defn decrypt-message
  "Decrypt a message using the simple substitution cipher. The function is case-insensitive. The substitution-table must be (int, symbols)."
  [message substitution-table]
  (let [message (cs/split message #",")]
    (->> message
         (map #(Integer/parseInt %))
         (decrypt-message-by-symbol-table substitution-table))))

(defn decrypt-message-with-caesar
  "Decrypt a message using the simple substitution cipher and the Caesar cipher. The function is case-insensitive."
  [message key substitution-table symbols]
  (-> message
      (decrypt-message substitution-table)
      (caesar/decrypt-message key symbols)))

