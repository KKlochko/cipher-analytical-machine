package cipher_analytical_machine.ciphers.caesar;

public interface Decrypted {
    String decrypt(String message, int key, String symbols);
}

