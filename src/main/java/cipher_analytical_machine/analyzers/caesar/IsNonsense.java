package cipher_analytical_machine.analyzers.caesar;

public interface IsNonsense {
    boolean isNonsense(String message);
}

