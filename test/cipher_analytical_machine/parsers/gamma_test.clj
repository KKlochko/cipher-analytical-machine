(ns cipher-analytical-machine.parsers.gamma-test
  (:require
   [clojure.test :refer :all]
   [cipher-analytical-machine.parsers.gamma :refer :all]))

(deftest key?-test
  (testing "The function return true only if the key in the format '%d,%d,%d'."
    (are [str expected]
        (= expected
           (key? str))
      "9"         false
      "9,10"      false
      "10,9,8"    true
      "-10,9,-8"  true
      "-10,-9,-8" true
      "asd 10 "   false
      " 10 "      false
      "-10"       false
      "abc"       false)))

