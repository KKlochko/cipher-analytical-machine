(ns cipher-analytical-machine.parsers.simple-substitution-test
  (:require
   [clojure.test :refer :all]
   [cipher-analytical-machine.parsers.simple-substitution :refer :all]))

(deftest encode-key-and-substitution-table-to-json-test
  (testing "The encoder must parse the map and create a json."
    (are [key table expected-json]
        (= expected-json
           (encode-key-and-substitution-table-to-json key table))
      1 {0 \a 1 \b 2 \c} "{\"key\":1,\"table\":{\"0\":\"a\",\"1\":\"b\",\"2\":\"c\"}}"
      2 {\a 0 \b 1 \c 2} "{\"key\":2,\"table\":{\"a\":0,\"b\":1,\"c\":2}}")))

(deftest decode-key-and-substitution-table-from-json-test
  (testing "The decoder must parse the json and create a map."
    (are [json expected-data]
        (= expected-data
           (decode-key-and-substitution-table-from-json json))
      "{\"key\":1, \"table\":{\"0\":\"a\",\"1\":\"b\",\"2\":\"c\"}}" {"key" 1 "table" {0 \a 1 \b 2 \c}}
      "{\"key\":2, \"table\":{\"a\":0,\"b\":1,\"c\":2}}" {"key" 2 "table" {\a 0 \b 1 \c 2}})))

(deftest generate-table-or-decode-json-test
  (let [symbols "abc"]
    (testing "The function decodes the json."
      (are [json expected-data]
          (= expected-data
             (generate-table-or-decode-json json symbols))
        "{\"key\":1, \"table\":{\"0\":\"a\",\"1\":\"b\",\"2\":\"c\"}}" {"key" 1 "table" {0 \a 1 \b 2 \c}}
        "{\"key\":2, \"table\":{\"a\":0,\"b\":1,\"c\":2}}" {"key" 2 "table" {\a 0 \b 1 \c 2}}))

    (testing "The function generates the table, because the argument is a key."
      (are [json expected-data]
          (let [data (generate-table-or-decode-json json symbols)]
            (and (= (get expected-data "key")
                    (get data "key"))
                 (= (keys (get expected-data "table"))
                    (keys (get data "table"))))
             (generate-table-or-decode-json json symbols))
        "1" {"key" 1 "table" {0 \a 1 \b 2 \c}}
        "2" {"key" 2 "table" {\a 0 \b 1 \c 2}}))))

(deftest invert-table-in-map-test
  (testing "The function invert the table in a decoded json."
    (are [decoded-json expected-data]
        (= expected-data
           (invert-table-in-map decoded-json))
      {"key" 1 "table" {0 \a 1 \b 2 \c}} {"key" 1 "table" {\a 0 \b 1 \c 2}}
      {"key" 2 "table" {\a 0 \b 1 \c 2}} {"key" 2 "table" {0 \a 1 \b 2 \c}})))

