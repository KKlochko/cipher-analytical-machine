(ns cipher-analytical-machine.parsers.parsers-test
  (:require
   [clojure.test :refer :all]
   [cipher-analytical-machine.parsers.parsers :refer :all]))

(deftest unsigned-int-string?-test
  (testing "The function return true only for an unsingned integer as a string."
    (are [str expected]
        (= expected
           (unsigned-int-string? str))
      "9" true
      "9" true
      "10" true
      "asd 10 " false
      " 10 " false
      "-10" false
      "abc" false)))

(deftest int-string?-test
  (testing "The function return true only for an singned integer as a string."
    (are [str expected]
        (= expected
           (int-string? str))
      "9" true
      "10" true
      "-10" true
      "asd -10 " false
      " -10 " false
      "abc" false)))

(deftest parse-unsigned-int-test
  (testing "The function parse the integer if the string is an integer."
    (are [str-or-int expected]
        (= expected
           (parse-unsigned-int str-or-int))
      "9"   9
      9     9
      "10"  10
      "-10" nil
      "abc" nil)))
