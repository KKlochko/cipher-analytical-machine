(ns cipher-analytical-machine.symbols.frequencies-test
  (:require
   [clojure.test :refer :all]
   [cipher-analytical-machine.symbols.frequencies :refer :all]))

(deftest map-to-string-test
  (let [symbol-map english-letter-frequencies]
    (testing "The map must be converted to 'etaoinsrhldcumfpgwybvkxjqz'"
      (is (= "etaoinsrhldcumfpgwybvkxjqz"
             (map-to-string symbol-map))))))

(deftest default-frequency-factory-test
  (testing "Factory supports languages: English (en), Ukrainian (uk). If language code is unidentified, then it must have the 'en'."
    (are [language-code expected-map-size]
        (->> (default-frequency-factory language-code)
             count
             (= expected-map-size))
      "en"           26
      "uk"           34
      "Maybe a code" 26)))

