(ns cipher-analytical-machine.symbols.factories-test
  (:require
   [clojure.test :refer :all]
   [cipher-analytical-machine.symbols.factories :refer :all]
  ))

(deftest english-alphabet-factory-test
  (let [symbols (english-alphabet-factory)]
    (testing "It must have the same order."
      (is (= "abcdefghijklmnopqrstuvwxyz" symbols)))

    (testing "It must have 26 letters."
      (is (= 26 (count symbols))))))

(deftest ukrainian-alphabet-factory-test
  (let [symbols (ukrainian-alphabet-factory)]
    (testing "It must have the same order."
      (is (= "абвгґдеєжзиіїйклмнопрстуфхцчшщьюя" symbols)))

    (testing "It must have 33 letters."
      (is (= 33 (count symbols))))))

(deftest digit-set-factory-test
  (let [symbols (digit-set-factory)]
    (testing "It must have the same order."
      (is (= "0123456789" symbols)))

    (testing "It must have 10 digits."
      (is (= 10 (count symbols))))))

(deftest punctuation-symbol-factory-test
  (let [symbols (punctuation-symbol-factory)]
    (testing "It must have the same order."
      (is (= ".,:;-!?" symbols)))

    (testing "It must have 7 symbols."
      (is (= 7 (count symbols))))))

(deftest space-symbol-factory-test
  (let [symbols (space-symbol-factory)]
    (testing "It must have the same order."
      (is (= " \t\n" symbols)))

    (testing "It must have 3 symbols."
      (is (= 3 (count symbols))))))

(deftest default-symbol-factory-test
  (testing "If language code is 'en', then it must have 27 symbols."
    (let [symbols (default-symbol-factory "en")]
      (is (= 27 (count symbols)))))

  (testing "If language code is 'uk', then it must have 34 symbols."
    (let [symbols (default-symbol-factory "uk")]
      (is (= 34 (count symbols)))))

  (testing "If language code is unidentified, then it must have the 'en'."
    (let [symbols (default-symbol-factory "Maybe a code")]
      (is (= 27 (count symbols))))))

