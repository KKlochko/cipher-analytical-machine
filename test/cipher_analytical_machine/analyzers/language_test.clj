(ns cipher-analytical-machine.analyzers.language-test
  (:require
   [clojure.test :refer :all]
   [cipher-analytical-machine.analyzers.language :refer :all]
  ))

(deftest detect-language-test
  (testing "Checking the English text"
    (is (= "en" (detect-language "This is a sentence that uses English."))))

  (testing "Checking the Ukrainian text"
    (is (= "uk" (detect-language "Текст, що написаний українською."))))

  (testing "Checking a gibberish that uses English letters"
    (is (= "lt" (detect-language "dfgjldfjgdfl gjdfg idfjig hdgesr khs e."))))

  (testing "Checking a gibberish that uses Ukrainian letters"
    (is (= "uk" (detect-language "іаврпшшцді врадів аргів аріл варї йцґ.")))))

(deftest is-language?-test
  (testing "Checking the English text"
    (is (is-language? "This is a sentence that uses English." "en")))

  (testing "Checking the Ukrainian text"
    (is (is-language? "Текст, що написаний українською." "uk")))

  (testing "Checking a gibberish that uses English letters"
    (is (is-language? "dfgjldfjgdfl gjdfg idfjig hdgesr khs e." "lt")))

  (testing "Checking a gibberish that uses Ukrainian letters"
    (is (is-language? "іаврпшшцді врадів аргів аріл варї йцґ." "uk"))))

(deftest is-nonsense?-test
  (testing "Checking the English text"
    (is (not (is-nonsense? "This is a sentence that uses English." "en"))))

  (testing "Checking the Ukrainian text"
    (is (not (is-nonsense? "Текст, що написаний українською." "uk"))))

  (testing "Checking a gibberish that uses English letters"
    (is (is-nonsense? "dfgjldfjgdfl gjdfg idfjig hdgesr khs e." "en")))

  (testing "Checking a gibberish that uses Ukrainian letters"
    (is (is-nonsense? "іаврпшшцді івафі лоінїмч йцувіа івапґ." "uk"))))

