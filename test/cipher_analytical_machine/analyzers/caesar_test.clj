(ns cipher-analytical-machine.analyzers.caesar-test
  (:require
   [clojure.test :refer :all]
   [cipher-analytical-machine.analyzers.caesar :refer :all]
   [cipher-analytical-machine.symbols.frequencies :as sf]
   [cipher-analytical-machine.analyzers.analyzers :as ca]
   [clojure.string :as cs]))

(deftest get-all-texts-test
  (testing "There're three text when the text is 'abc' and the set of symbol is 'abc'."
    (is (= 3 (count (get-all-texts "abc" "abc")))))

  (testing "A pair must have an integer and a string."
    (let [pairs (get-all-texts "abc" "abc")
          pair (first pairs)]
      (is (and (int? (first pair))
               (string? (last pair))))))

  (testing "There're 27 combinations for a 'Hello World'."
    (let [ciphertext "khoorczruog"
        symbols "abcdefghijklmnopqrstuvwxyz "]
      (is (= 27 (count (get-all-texts ciphertext symbols)))))))

(deftest get-all-scores-test
  (testing "There're 27 combinations for a 'Hello World'."
    (let [ciphertext "khoorczruog"
          symbols "abcdefghijklmnopqrstuvwxyz "
          frequencies sf/english-letter-frequencies
          text-pairs (get-all-texts ciphertext symbols)]
      (is (= 27 (count (get-all-scores frequencies text-pairs)))))))

(deftest get-min-score-pair-test
  (testing "If a key is one, then the min score pair is 15."
    (let [scores (list [0 20.0] [1 15.0] [2 30.0])
          min-score-pair (get-min-score-pair scores)]
      (is (= [1 15.0] min-score-pair)))))

(deftest get-key-test
  (let [plaintext "hello world"
        ciphertext "khoorczruog"
        key 3
        symbols "abcdefghijklmnopqrstuvwxyz "
        frequencies sf/english-letter-frequencies]
    (testing "The plaintext is encrypted with 3 as the key."
      (is (= key (get-key ciphertext symbols frequencies)))))

  (let [; З поеми "Кавказ" Тараса Григоровича Шевченка:
        plaintext "Борітеся – поборете, Вам Бог помагає! За вас правда, за вас слава. І воля святая!"
        ciphertext "жхчощйшде–ецхжхчйщй,езєуежхиецхуєиєк!емєезєшецчєзїє,емєезєшештєзє.еоезхтдешздщєд!"
        key 7
        symbols "абвгґдеєжзиіїйклмнопрстуфхцчшщьюя "
        frequencies sf/ukrainian-letter-frequencies]
    (testing "The plaintext is encrypted with 3 as the key."
      (is (= key (get-key ciphertext symbols frequencies))))))

(deftest get-plaintext-test
  (let [plaintext "hello world"
        ciphertext "khoorczruog"
        symbols "abcdefghijklmnopqrstuvwxyz "
        frequencies sf/english-letter-frequencies]
    (testing "The plaintext is encrypted with 3 as the key."
      (is (= plaintext (get-plaintext ciphertext symbols frequencies))))

    (testing "The ciphertext is case-insensitive."
      (is (= plaintext (get-plaintext "KhoorcZruog" symbols frequencies)))))

  (let [; З поеми "Кавказ" Тараса Григоровича Шевченка:
        plaintext "Борітеся – поборете, Вам Бог помагає! За вас правда, за вас слава. І воля святая!"
        ciphertext "жхчощйшде–ецхжхчйщй,езєуежхиецхуєиєк!емєезєшецчєзїє,емєезєшештєзє.еоезхтдешздщєд!"
        key 7
        symbols "абвгґдеєжзиіїйклмнопрстуфхцчшщьюя "
        frequencies sf/ukrainian-letter-frequencies]
    (testing "The ciphertext is case-insensitive."
      (is (= (cs/lower-case plaintext)
             (get-plaintext ciphertext symbols frequencies))))))

