(ns cipher-analytical-machine.analyzers.analyzers-test
  (:require
   [clojure.test :refer :all]
   [cipher-analytical-machine.analyzers.analyzers :refer :all]))

(deftest count-characters-test
  (testing "Count two dublicates and one uniq character from a string."
    (is (= {\a 2 \b 1}
           (count-characters "aba")))))

(deftest reverse-count-characters-test
  (testing "The symbols must be grouped as a vector and the count must be a key."
    (are [count-map expected-map]
        (= expected-map (reverse-count-characters count-map))
      {\a 1 \b 2 \c 1} {1 [\a \c] 2 [\b]}
      {\a 1 \b 2 \c 3} {1 [\a] 2 [\b] 3 [\c]})))

(deftest chi-squared-letter-statistic-test
  (testing "If the frequence of A is 0.1, the text length is 100 and contains 20 times, then the chi-squared is 10."
    (is (= 10.0
           (chi-squared-letter-statistic 20 0.1 100))))

  (testing "If the frequence of A is 0.1, the text length is 10 and contains 5 times, then the chi-squared is 16.0."
    (is (= 16.0
           (chi-squared-letter-statistic 5 0.1 10)))))

(deftest chi-squared-statistic-test
  (let [text "aaaaabbbbb"]
  (testing "If the frequencies of a and b is 0.1, the text length is 10 and contains them 5 times, then score is 32."
    (is (= 32.0
           (chi-squared-statistic text {\a 0.1 \b 0.1}))))))

(deftest sort-map-by-count-test
  (testing "Test the sort in descending order."
    (is (= [[\b 3] [\a 2] [\c 1]]
           (sort-map-by-count {\a 2 \b 3 \c 1} )))))

(deftest table-to-string-test
  (testing "Test the sort in descending order."
    (is (= "bac"
           (table-to-string [[\b 3] [\a 2] [\c 1]])))))

(deftest symbol-frequency-table-test
  (testing "Test the sort in descending order."
    (is (= {\d \b, \e \a, \f \c}
           (symbol-frequency-table {\d 3 \e 2 \f 1} {\b 3 \a 2 \c 1})))))

