(ns cipher-analytical-machine.analyzers.simple-substitution-test
  (:require
   [clojure.test :refer :all]
   [cipher-analytical-machine.symbols.frequencies :as sf]
   [cipher-analytical-machine.ciphers.simple-substitution :as ss]
   [cipher-analytical-machine.analyzers.simple-substitution :refer :all])
  (:gen-class))

(deftest get-possible-keys-vector-test
  (testing "The vector must be ordered by symbols"
    (are [possible-keys-map symbols expected-key-vector]
        (= expected-key-vector (get-possible-key-vector possible-keys-map symbols))
      {\a [\a \b] \b [\b \a] \c [\a \c]} "abc" [[\a \b] [\b \a] [\a \c]]
      {\a [\a \b] \c [\b \a] \b [\a \c]} "abc" [[\a \b] [\a \c] [\b \a]])))

(deftest get-possible-key-vector-from-reversed-count-map-test
  (testing "The block order must be in descending order"
    (are [reversed-count-map expected-key-vector]
        (= expected-key-vector (get-possible-key-vector-from-reversed-count-map reversed-count-map))
      {1 [\a \c] 2 [\b]}     [[\b] [\a \c]]
      {2 [\a] 1 [\b] 3 [\c]} [[\c] [\a] [\b]])))

(deftest get-all-permutation-for-block-test
  (testing "It must return all permutations"
    (are [symbols expected-permutations]
        (= expected-permutations (get-all-permutation-for-block symbols))
      [\a \b]    ["ab" "ba"]
      [\a \b \c] ["abc" "acb" "bac" "bca" "cab" "cba"])))

(deftest get-all-combinations-for-blocks-test
  (testing "The keys must be the all combinations of blocks"
    (are [possible-keys-vector expected-keys]
        (= expected-keys (get-all-combinations-for-blocks possible-keys-vector))
      [[\a \b] [\f] [\a \c] [\g]] '("afag" "afcg" "bfag" "bfcg")
      [[\a \b] [\a \c]] '("aa" "ac" "ba" "bc")
      [[\a \b \c] [\a \c] [\b \c]] '("aab" "aac" "acb" "acc" "bab" "bac" "bcb" "bcc" "cab" "cac" "ccb" "ccc"))))

(deftest get-possible-key-combinations-test
  (testing "The keys must be the all combinations, but the blocks must create a permutations which will be joined as a combination"
    (are [possible-keys-vector expected-keys]
        (= expected-keys (get-possible-key-combinations possible-keys-vector))
      [[\a \b] [\f] [\e \c] [\g]] '("abfecg" "abfceg" "bafecg" "bafceg")
      [[\a \b] [\a \c]] '("abac" "abca" "baac" "baca")
      [[\a \b \c] [\a \c] [\b \c]] '("abcacbc" "abcaccb" "abccabc" "abccacb"
                                     "acbacbc" "acbaccb" "acbcabc" "acbcacb"
                                     "bacacbc" "bacaccb" "baccabc" "baccacb"
                                     "bcaacbc" "bcaaccb" "bcacabc" "bcacacb"
                                     "cabacbc" "cabaccb" "cabcabc" "cabcacb"
                                     "cbaacbc" "cbaaccb" "cbacabc" "cbacacb"))))

