(ns cipher-analytical-machine.ciphers.caesar-test
  (:require
   [clojure.test :refer :all]
   [cipher-analytical-machine.ciphers.caesar :refer :all]
  ))

(deftest calculate-char-index-test
  (let [symbols "abc123%$"]
    (testing "Letter 'b' in symbols"
      (is (= 1 (calculate-char-index \b symbols))))

    (testing "Letter 'd' does not in symbols"
      (is (= nil (calculate-char-index \d symbols))))

    (testing "Digit '1' in symbols"
      (is (= 3 (calculate-char-index \1 symbols))))

    (testing "Digit '0' does not in symbols"
      (is (= nil (calculate-char-index \0 symbols))))

    (testing "Symbol '?' does not in symbols"
      (is (= nil (calculate-char-index \? symbols))))))

(deftest encrypt-index-test
  (testing "The new index is in the range."
    (is (= 2 (encrypt-index 0 2 33))))

  (testing "The new index that equal to max-index."
    (is (= 0 (encrypt-index 30 3 33))))

  (testing "The new index that greater than max-index."
    (is (= 1 (encrypt-index 30 4 33))))

  (testing "The new index that lower than 0."
    (is (= 32 (encrypt-index 0 -1 33)))))

(deftest encrypt-char-test
  (let [symbols "abc"
        max-index (count symbols)]
    (testing "Letter 'a' is encrypted to 'b'"
      (is (= \b (encrypt-char \a 1 symbols max-index))))

    (testing "Letter 'b' is encrypted to 'a'"
      (is (= \a (encrypt-char \b 2 symbols max-index))))

    (testing "Letter 'b' is decryped to 'a'"
      (is (= \a (encrypt-char \b -1 symbols max-index))))))

(deftest encrypt-message-text
  (let [symbols "abc"]
    (testing "The message 'abc' is encrypted to 'bca'"
      (is (= "bca" (encrypt-message "abc" 1 symbols))))

    (testing "The message 'ABC' is encrypted to 'bca', because of case-insensitive."
      (is (= "bca" (encrypt-message "ABC" 1 symbols))))

    (testing "The message 'bca' is decrypted to 'abc'"
      (is (= "abc" (encrypt-message "bca" -1 symbols))))))

(deftest decrypt-message-text
  (let [symbols "abc"]
    (testing "The message 'bca' is decrypted to 'abc'"
      (is (= "abc" (decrypt-message "bca" 1 symbols))))

    (testing "The message 'BCA' is decrypted to 'abc', because of case-insensitive."
      (is (= "abc" (decrypt-message "BCA" 1 symbols))))

    (testing "The message 'abc' is encrypted to 'bca'"
      (is (= "bca" (decrypt-message "abc" -1 symbols))))))

