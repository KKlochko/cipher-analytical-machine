(ns cipher-analytical-machine.ciphers.simple-substitution-test
  (:require
   [clojure.test :refer :all]
   [cipher-analytical-machine.ciphers.simple-substitution :refer :all]))

(deftest find-value-in-table-test
  (let [table {\a 1 \b 2 \c 3}
        rtable {1 \a 2 \b 3 \c}]
    (testing "If the symbol is in the table as a key, then the result won't nil."
      (are [key expected]
          (= expected (find-value-in-table key table))
        \a 1
        \d nil
        1 nil
        5 nil))

    (testing "If the digit is in the reversed table as a key, then the result won't nil."
      (are [key expected]
          (= expected (find-value-in-table key rtable))
        \a nil
        \d nil
        1 \a
        5 nil))))

(deftest generate-sorted-substitution-table-test
  (testing "If the symbol is in the table as a key, then the result won't nil."
    (are [symbols expected-table]
        (= expected-table (generate-sorted-substitution-table symbols))
      "abc" {0 \a 1 \b 2 \c}
      "a" {0 \a})))

(deftest encrypt-message-test
  (let [symbols "abc"
        table {\a 1 \b 2 \c 3}]
    (testing "The function must encrypt the message and remove unknown symbols."
      (are [message expected]
          (= expected (encrypt-message message table))
        "abc" "1,2,3"
        "aDbdc" "1,2,3"))))

(deftest encrypt-message-with-caesar-test
  (let [symbols "abc"
        table {\a 1 \b 2 \c 3}]
    (testing "The function must encrypt the message and remove unknown symbols."
      (are [message key expected]
          (= expected (encrypt-message-with-caesar message key table symbols))
        "abc" 0 "1,2,3"
        "abc" 1 "2,3,1"
        "aDbdc" 0 "1,2,3"))))

(deftest decrypt-by-table-test
  (let [table {\d \a \e \b \f \c}]
    (testing "The function must decrypt the message by the table."
      (are [message expected]
          (= expected (decrypt-by-table table message))
        "def" '(\a \b \c)
        "abf" '(nil nil \c)
        "fed" '(\c \b \a)))))

(deftest decrypt-message-by-symbol-table-test
  (let [table {\d \a \e \b \f \c}]
    (testing "The function must decrypt the message and remove unknown numbers."
      (are [message expected]
          (= expected (decrypt-message-by-symbol-table table message))
        "def" "abc"
        "daef" "abc"))))

(deftest decrypt-message-test
  (let [table {1 \a 2 \b 3 \c}]
    (testing "The function must decrypt the message and remove unknown numbers."
      (are [message expected]
          (= expected (decrypt-message message table))
        "1,2,3" "abc"
        "1,12,2,3" "abc"))))

(deftest decrypt-message-with-caesar-test
  (let [symbols "abc"
        table {1 \a 2 \b 3 \c}]
    (testing "The function must decrypt the message and remove unknown numbers."
      (are [message key expected]
          (= expected (decrypt-message-with-caesar message key table symbols))
        "1,2,3" 0 "abc"
        "2,3,1" 1 "abc"
        "1,12,2,3" 0 "abc"))))

