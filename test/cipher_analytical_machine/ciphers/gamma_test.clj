(ns cipher-analytical-machine.ciphers.gamma-test
  (:require
   [clojure.test :refer :all]
   [cipher-analytical-machine.ciphers.gamma :refer :all]))

(deftest add-mod-test
  (testing "The remainder must be as expected."
    (are [a b module expected]
        (= expected (add-mod a b module))
      5 2 4 3
      1 2 3 0
      2 2 3 1)))

(deftest generate-seq-test
  (testing "Checking that the next element of a sequence is calculated by (e[i-1] + e[i-3] % mod)."
    (are [a b c module size expected]
        (= expected (take size (generate-seq a b c module)))
      4 32 15 33 10 [4 32 15 19 18 0 19 4 4 23]
      1 2  3  3  9  [1 2 3 1 0 0 1 1 1])))

(deftest generate-gamma-seq-test
  (testing "Checking that the next element of a sequence is calculated by (s[i] + s[i+1] % mod)."
    (are [acc seq module expected]
        (= expected (generate-gamma-seq acc seq module))
      [] [4 32 15 19 18 0 19 4 4 23] 33 [3 14 1 4 18 19 23 8 27]
      [] [1 2 3 1 0 0 1 1 1]         3  [0 2 1 1 0 1 2 2])))

(deftest generate-gamma-test
  (testing "Checking that the gamma is generated as expected."
    (are [a b c module size expected]
        (= expected (generate-gamma a b c module size))
      4 32 15 33 10 [3 14 1 4 18 19 23 8 27 17]
      1 2  3  3  9  [0 2 1 1 0 1 2 2 0])))

(deftest encrypt-array-test
  (testing "Checking that the message is encrypted as expected: (msg+gamma)%module."
    (are [key module array expected]
        (= expected (encrypt-array key module array))
      [4 32 15] 33 [3 14 1 4 18 19 23 8 27 17] [6 28 2 8 3 5 13 16 21 1]
      [1 2  3 ] 3  [0 2 1 1 0 1 2 2 0]         [0 1 2 2 0 2 1 1 0])))

(deftest encrypt-message-test
  (testing "Checking that the message is encrypted as expected"
    (are [key symbols message expected]
        (= expected (encrypt-message key symbols message))
      [4 32 15] "абвгґдеєжзиіїйклмнопрстуфхцчшщьюя" "гкбґопужчн" "ешвжгдймсб"
      [1 2  3 ] "абв"  "авббабвва"  "абввавбба")))

(deftest decrypt-array-test
  (testing "Checking that the message is decrypted as expected."
    (are [key module array expected]
        (= expected (decrypt-array key module array))
      [4 32 15] 33 [6 28 2 8 3 5 13 16 21 1] [3 14 1 4 18 19 23 8 27 17]
      [1 2  3 ] 3  [0 1 2 2 0 2 1 1 0]       [0 2 1 1 0 1 2 2 0])))

(deftest decrypt-message-test
  (testing "Checking that the message is decrypted as expected"
    (are [key symbols message expected]
        (= expected (decrypt-message key symbols message))
      [4 32 15] "абвгґдеєжзиіїйклмнопрстуфхцчшщьюя" "ешвжгдймсб" "гкбґопужчн"
      [1 2  3 ] "абв" "абввавбба" "авббабвва")))

